# Copyright 2021 Alex Elsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

SCM_REPOSITORY="http://svn.osdn.net/svnroot/${PN}/"
SCM_SUBPATH="caitsith-patch"

if ever is_scm; then
    require scm-svn
fi

SUMMARY="A simplified access restriction module for system protection"
DESCRIPTION="
Characteristic action inspection tool. See if this helps.

CaitSith is an LSM designed to allow flexible, incrementally developable
security policies for complex Linux systems.
"
HOMEPAGE="http://caitsith.osdn.jp"
if ! ever is_scm; then
    DOWNLOADS="https://osdn.net/frs/redir.php?m=tuna&f=${PN}/66537/${PN}-patch-${PV/p/}.tar.gz -> ${PNV}.tar.gz"
fi

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
    build+run:
"

src_unpack() {
    if ever is_scm; then
        default

        scm_src_unpack
    else
        edo mkdir ${WORK}
        cd ${WORK}
        unpack ${ARCHIVES}
    fi
}

src_install() {
    insinto /usr/src
    doins -r caitsith

    cat <<'KERNEL_INSTALL_BUILDER' > 20-caitsith.install
#!/bin/bash
# -*- mode: shell-script; indent-tabs-mode: nil; sh-basic-offset: 4; -*-
# ex: ts=8 sw=4 sts=4 et filetype=sh

COMMAND="$1"
KERNEL_VERSION="$2"
BOOT_DIR_ABS="$3"
KERNEL_IMAGE="$4"
KERNEL_DIR="$(dirname "${KERNEL_IMAGE}")"

[[ -n "${KERNEL_VERSION}" ]] || exit 77

ensure() {
    local DIAGNOSTIC="$1"; shift

    "$@"

    if [[ $? != 0 ]]; then
        echo "Unable to ensure ${DIAGNOSTIC}" >&2
        exit 77
    fi
}

case "${COMMAND}" in
    add)
        ensure "CaitSith is built"                         \
            make                                           \
                -j "$(nproc)"                              \
                -C /usr/src/linux/"${KERNEL_VERSION}"      \
                ARCH="$(readlink /usr/host | cut -d- -f1)" \
                CROSS_COMPILE="$(readlink /usr/host)-"     \
                HOSTCC="$(readlink /usr/host)-cc"          \
                HOSTCXX="$(readlink /usr/host)-c++"        \
                KCONFIG_NOSILENTUPDATE=1                   \
                M=/usr/src/caitsith                        \
                KCONFIG_CONFIG=/etc/kernel/preserved/"${KERNEL_VERSION}"/current/config \
                modules

        ensure "CaitSith is installed"                     \
            make                                           \
                -j "$(nproc)"                              \
                -C /usr/src/linux/"${KERNEL_VERSION}"      \
                ARCH="$(readlink /usr/host | cut -d- -f1)" \
                CROSS_COMPILE="$(readlink /usr/host)-"     \
                HOSTCC="$(readlink /usr/host)-cc"          \
                HOSTCXX="$(readlink /usr/host)-c++"        \
                KCONFIG_NOSILENTUPDATE=1                   \
                M=/usr/src/caitsith                        \
                KCONFIG_CONFIG=/etc/kernel/preserved/"${KERNEL_VERSION}"/current/config \
                modules_install
        ;;
    remove)
        rm -rf /lib/modules/"${KERNEL_VERSION}"
        ;;
    *)
        exit 0
esac
KERNEL_INSTALL_BUILDER
    exeinto /usr/$(exhost --target)/lib/kernel/install.d
    doexe 20-caitsith.install
}

